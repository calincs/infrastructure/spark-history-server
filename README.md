# Spark History Server

This project deploys a Spark History Server to monitor jobs on the LINCS Spark cluster.

Spark image pulled from here: [https://hub.docker.com/r/datamechanics/spark](https://hub.docker.com/r/datamechanics/spark).

## SHS Service account

The gitlab CI service account does not have the permission to create the SHS service account RBAC permissions at cluster scope. You will need to create the service account, RBAC role, and binding manually before deploying to the cluster. Replace the `namespace` in the script below:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: spark-history-service
  namespace: spark-history-server-26802763-production
  labels:
    app.kubernetes.io/name: spark-history
    helm.sh/chart: spark-history-server
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: spark-history-cr
  namespace: spark-history-server-26802763-production
  labels:
    app.kubernetes.io/name: spark-history
    helm.sh/chart: spark-history-server
rules:
- apiGroups: [""]
  resources: ["deployments", "pods"]
  verbs: ["*"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: spark-history-crb
  namespace: spark-history-server-26802763-production
subjects:
- kind: ServiceAccount
  name: spark-history-service
  namespace: spark-history-server-26802763-production
roleRef:
  kind: ClusterRole
  name: spark-history-cr
  apiGroup: rbac.authorization.k8s.io
```
